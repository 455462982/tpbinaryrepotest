//
//  TPCustomManager.h
//  TPDependencyExample
//
//  Created by sfit0194 on 3/15/17.
//  Copyright © 2017 sfit0194. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TPCustomManager : NSObject

@property (nonatomic , readonly) NSString *version;

- (CGFloat)imageSize;

@end
