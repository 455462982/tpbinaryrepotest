Pod::Spec.new do |s|

s.name         = 'TPCustomComponent'
s.version      = '0.0.1'
s.summary      = 'TPCustomComponent for iOS.'

s.description  = <<-DESC
TPCustomComponent SDK for iOS by tengpan.
DESC

s.homepage     = 'https://github.com/Tengpan/TPDependencyExample'

s.license = { :type => 'Copyright', :text => <<-LICENSE
Copyright © 2017 tengpan. All Rights Reserved.
LICENSE
}

s.authors      = { '滕盼' => '455462982@qq.com' }
s.source       = { :git => "https://455462982@bitbucket.org/455462982/tpbinaryrepotest.git", :tag => s.version}
s.platform     = :ios , '8.0'
s.source_files = 'TPCustomComponent.framework/Headers/*.h'
s.requires_arc = true
s.public_header_files     = 'TPCustomComponent.framework/Headers/*.h'
s.ios.vendored_frameworks = 'TPCustomComponent.framework'
s.frameworks   = 'Foundation' , 'UIKit'

end
